﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour {
	int max = 1000;
	int min = 1;
	int guess = 500;
	public int maxGuessAllowed;
	public Text areaText;
	
	void Start () {
		maxGuessAllowed = Random.Range(5,16);
		StartGame();
	}
	
	public void GuessHigher(){
		min=guess;
		NextGuess();
	}

	public void GuessLower(){
		max=guess;
		NextGuess();
	}

	void StartGame(){
		max = 1000;
		min = 1;
		guess = Random.Range(min,max+1);
		areaText.text = guess.ToString();
	}
	
	void NextGuess(){
		guess = Random.Range(min,max+1);
		maxGuessAllowed-=1;
		areaText.text = guess.ToString();
		if(maxGuessAllowed<=0){
			Application.LoadLevel("Win");
		}
	}
}